package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RecordarUsuario extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    Conexion conexion;
    ArrayAdapter<String> adaptador;
    ListView lista;
    boolean b = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordar_usuario);
        getSupportActionBar().hide();

        conexion = new Conexion(this);

        lista = (ListView)findViewById(R.id.listViewUsuarios);
        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, getLista());
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        b = true;
        SharedPreferences.Editor editor = getSharedPreferences("doggylove",MODE_PRIVATE).edit();
        editor.putBoolean("recordarusuario",b);
        editor.commit();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Perro.CAMPO_ID, Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO};
        String[] valoresWhere = {listaUsuarios.get(i).getUsuario()};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_USUARIO+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        Perro perro = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),new Usuario(listaUsuarios.get(i).getUsuario(),null));

        Intent intent = new Intent(this,IniciarSesionActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable("perro",perro);
        intent.putExtras(extras);
        startActivity(intent);
    }


    List<Usuario> listaUsuarios;

    private List<String> getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Usuario.CAMPO_USUARIO,Usuario.CAMPO_CONTRASEÑA};

        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,null,null,null,null,Usuario.CAMPO_USUARIO);

        while (cursor.moveToNext()) {

            Usuario u = new Usuario(cursor.getString(0),cursor.getString(1));
            listaUsuarios.add(u);
            listaNombres.add(u.getUsuario());

        }
        return listaNombres;
    }


}