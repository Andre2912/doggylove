package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

public class ContactarPerro extends AppCompatActivity implements View.OnClickListener {
    private ImageView imagen;
    private TextView dueño;
    private TextView nombre;
    private TextView edad;
    private TextView raza;
    private TextView sexo;
    private TextView tamaño;
    Conexion conexion;
    byte[] blob;
    Button contactar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactar_perro);

        getSupportActionBar().hide();

        imagen = findViewById(R.id.imageView5);
        dueño = findViewById(R.id.textView25);
        nombre = findViewById(R.id.textView17);
        edad = findViewById(R.id.textView24);
        raza = findViewById(R.id.textView26);
        sexo = findViewById(R.id.textView23);
        tamaño = findViewById(R.id.textView28);

        contactar = findViewById(R.id.buttonContactar);
        contactar.setOnClickListener(this);

        conexion = new Conexion(this);

        Bundle extras = getIntent().getExtras();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO,Perro.CAMPO_FOTO,Perro.CAMPO_USUARIO};
        String[] valoresWhere = {extras.getInt("id")+""};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_ID+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        Perro perro = new Perro(extras.getInt("id"),cursor.getString(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),new Usuario(cursor.getString(6),null));
        if (cursor.getBlob(5) !=null) {
            this.blob = cursor.getBlob(5);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blob,0, blob.length);
            imagen.setImageBitmap(bitmap);
        }
        dueño.setText("Dueño: " + perro.getUsuario().getUsuario());
        nombre.setText("Nombre: " + perro.getNombre());
        edad.setText("Edad: " + perro.getEdad() + " año(s)");
        raza.setText("Raza: " + perro.getRaza());
        sexo.setText("Sexo: " + perro.getSexo());
        tamaño.setText("Tamaño: " + perro.getTamaño());

    }

    @Override
    public void onClick(View view) {

        Bundle extras = getIntent().getExtras();
        int id = extras.getInt("id");

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO,Perro.CAMPO_FOTO,Perro.CAMPO_USUARIO};
        String[] valoresWhere = {id+""};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_ID+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        Perro perro = new Perro(extras.getInt("id"),cursor.getString(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),new Usuario(cursor.getString(6),null));


        if (view.getId()==contactar.getId()) {
            Intent i = new Intent(this,Chat.class);
            Bundle extra = new Bundle();
            extra.putSerializable("datosperro",perro);
            i.putExtras(extra);
            startActivity(i);
        }





    }
}