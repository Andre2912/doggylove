package com.example.doggylove.entidades;

import com.example.doggylove.BaseDatos.Usuario;

import java.util.LinkedList;
import java.util.List;

public class GestiónDeUsuarios {
    List<Usuario> usuarios;

    public GestiónDeUsuarios() {

        String[] nombres = {"Valeria","Olga","Andre"};
        String[] contraseñas = {"2004","1976","2001"};
        usuarios = new LinkedList<Usuario>();

        for (int i=0;i< nombres.length;i++) {
            usuarios.add(new Usuario(nombres[i],contraseñas[i]));
        }

    }
    public boolean validarusuario(Usuario usuario) {
        boolean e = false;
        for (Usuario u : usuarios) {
            e = u.equals(usuario);
            if (e) {
                break;
            }
        }
        return e;
    }

    public boolean validarusuario(String usuario,String contraseña) {
        return validarusuario(new Usuario(usuario,contraseña));
    }

    public  boolean buscarUsuario(String usuario) {
        boolean e = false;
        for (Usuario u : usuarios) {
            e = u.getUsuario().equals(usuario);
            if (e) {
                break;
             }

        }
        return e;
    }

    public void agregarUsuario(String usuario,String contraseña) {
        usuarios.add(new Usuario(usuario,contraseña));
    }

}
