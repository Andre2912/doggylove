package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;

public class Chat extends AppCompatActivity implements View.OnClickListener {

    private ImageView foto,enviar;
    Conexion conexion;
    byte[] blob;
    private TextView usuario, mensaje2;
    private EditText mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().hide();

        conexion = new Conexion(this);

        foto = findViewById(R.id.imageView9);
        mensaje = findViewById(R.id.editTextMensaje);
        mensaje2 = findViewById(R.id.textView31);
        enviar = findViewById(R.id.imageView10);
        enviar.setOnClickListener(this);

        Bundle extra = getIntent().getExtras();
        Perro perro = (Perro) extra.getSerializable("datosperro");

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Perro.CAMPO_FOTO};
        String[] valoresWhere = {perro.getId()+""};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_ID+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        if (cursor.getBlob(0) !=null) {
            this.blob = cursor.getBlob(0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blob,0, blob.length);
            foto.setImageBitmap(bitmap);
        }

        usuario = findViewById(R.id.textView29);
        usuario.setText(perro.getUsuario().getUsuario());

    }

    @Override
    public void onClick(View view) {

        String msj = mensaje.getText().toString();

        if (view.getId() == enviar.getId()) {
            mensaje2.setText(msj);
            mensaje2.setVisibility(View.VISIBLE);
            mensaje.setText("");
        }
    }
}