package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;
import com.example.doggylove.entidades.GestiónDeUsuarios;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class IniciarSesionActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editContraseña, editUsuario;
    private Button IniciarSesion2,CuentasRegistradas;
    Conexion conexion;
    private Boolean b = false;
    private Boolean recordarsesion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
        getSupportActionBar().hide();
        IniciarSesion2 = findViewById(R.id.buttonIniciarSesion2);
        IniciarSesion2.setOnClickListener(this);
        editContraseña = findViewById(R.id.editTextContraseña);
        editUsuario = findViewById(R.id.editTextUsuario);
        CuentasRegistradas = findViewById(R.id.buttonCuentas);
        CuentasRegistradas.setOnClickListener(this);

        SharedPreferences sharedPreferences = getSharedPreferences("doggylove",MODE_PRIVATE);
        editUsuario.setText(sharedPreferences.getString("usuario_nombre",""));
        b = sharedPreferences.getBoolean("recordarusuario",false);

        if (b) {
            Bundle extras = getIntent().getExtras();
            Perro perro = (Perro) extras.getSerializable("perro");
            Usuario u = perro.getUsuario();
            editUsuario.setText(u.getUsuario());
            editUsuario.setEnabled(false);
            b = false;
            SharedPreferences.Editor editor = getSharedPreferences("doggylove",MODE_PRIVATE).edit();
            editor.putBoolean("recordarusuario",b);
            editor.commit();

        }
        conexion = new Conexion(this);

    }

    @Override
    public void onClick(View view) {
        String eUsuario = editUsuario.getText().toString();
        String eContraseña = editContraseña.getText().toString();
        Usuario user = new Usuario(eUsuario,eContraseña);
        List<Usuario> usuarios = getUsuarios();

        if (view.getId()  == R.id.buttonCuentas) {
            Intent c = new Intent(this,RecordarUsuario.class);
            startActivity(c);
        }
        if (view.getId() == R.id.buttonIniciarSesion2)

            if (eUsuario.equals("") || eContraseña.equals("")) {
                Toast.makeText(this, "Debe ingresar usuario y contraseña", Toast.LENGTH_LONG).show();
            }
            else if (validarUsuario(usuarios,user)) {
                recordarsesion = true;
                SharedPreferences.Editor editor = getSharedPreferences("doggylove",MODE_PRIVATE).edit();
                editor.putString("usuario_nombre",eUsuario);
                editor.putString("usuario_contraseña",eContraseña);
                editor.putBoolean("recordarsesion",recordarsesion);

                editor.commit();

                SQLiteDatabase db = conexion.getReadableDatabase();
                String[] campos = {Perro.CAMPO_ID, Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO};
                String[] valoresWhere = {eUsuario};
                Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_USUARIO+"=?",valoresWhere,null,null,null);
                cursor.moveToFirst();
                Perro perro = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),user);

                Intent i = new Intent(this,Home.class);
                Bundle extras = new Bundle();
                extras.putSerializable("perro",perro);
                i.putExtras(extras);
                startActivity(i);

            } else {
                Toast.makeText(this,"Usuario y/o contraseña incorrectas. Verificar que haya ingresado sus datos correctamente",Toast.LENGTH_LONG).show();
                editContraseña.setText("");
            }
    }

    List<Usuario> listaUsuarios;
    private List<Usuario> getUsuarios() {

        listaUsuarios = new LinkedList<Usuario>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Usuario.CAMPO_USUARIO,Usuario.CAMPO_CONTRASEÑA};

        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,null,null,null,null,Usuario.CAMPO_USUARIO);

        while (cursor.moveToNext()) {

            Usuario u = new Usuario(cursor.getString(0),cursor.getString(1));
            listaUsuarios.add(u);

        }
        return listaUsuarios;
    }

    private boolean validarUsuario(List<Usuario> a, Usuario u) {
        boolean b = false;
        for (Usuario usuario : a) {
            b = usuario.equals(u);
            if (b) {
                break;
            }
        }
        return b;
    }

}