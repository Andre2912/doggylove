package com.example.doggylove.BaseDatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {

    public static final String BD_APP = "bd_usuarios";

    public Conexion(@Nullable Context context) {
        super(context,BD_APP,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Usuario.CREAR_TABLA_USUARIOS);
        sqLiteDatabase.execSQL(Perro.CREAR_TABLA_PERROS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Usuario.DROPEAR_TABLA_USUARIOS);
        sqLiteDatabase.execSQL(Perro.DROPEAR_TABLA_PERROS);
        onCreate(sqLiteDatabase);
    }
}
