package com.example.doggylove.BaseDatos;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.BitSet;

public class Perro implements Serializable {

    private int id;
    private String nombre;
    private int edad;
    private String raza;
    private String sexo;
    private String tamaño;
    private Usuario usuario;
    private Bitmap imagen;

    public Perro(int id, String nombre, int edad, String raza, String sexo, String tamaño, Usuario usuario) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.sexo = sexo;
        this.tamaño = tamaño;
        this.usuario = usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public static final String TABLA_PERROS = "perros";
    public static  final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_EDAD = "edad";
    public static final String CAMPO_RAZA = "raza";
    public static final String CAMPO_SEXO = "sexo";
    public static final String CAMPO_TAMAÑO = "tamaño";
    public static final String CAMPO_USUARIO = "usuario";
    public static final String CAMPO_FOTO = "foto";


    public static final String CREAR_TABLA_PERROS = "CREATE TABLE " + TABLA_PERROS + " (" +
            CAMPO_ID + " INTEGER NOT NULL PRIMARY KEY, " +
            CAMPO_NOMBRE + " TEXT, " +
            CAMPO_EDAD + " INTEGER, "+
            CAMPO_RAZA + " TEXT, "+
            CAMPO_SEXO + " TEXT, "+
            CAMPO_TAMAÑO + " TEXT, "+
            CAMPO_FOTO + " BLOB, "+
            CAMPO_USUARIO + " TEXT, "+
            "FOREIGN KEY (" + CAMPO_USUARIO + ") REFERENCES " + Usuario.TABLA_USUARIOS + " (" + Usuario.CAMPO_USUARIO + ")); ";

    public static final String DROPEAR_TABLA_PERROS ="DROP TABLE IF EXISTS " + TABLA_PERROS;
}
