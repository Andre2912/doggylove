package com.example.doggylove.BaseDatos;

import java.io.Serializable;
import java.util.List;

public class Usuario implements Serializable {
    private String usuario;
    private String contraseña;

    public Usuario(String usuario, String contraseña) {
        this.usuario = usuario;
        this.contraseña = contraseña;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CAMPO_USUARIO = "usuario";
    public static final String CAMPO_CONTRASEÑA = "contraseña";

    public static final String CREAR_TABLA_USUARIOS = "CREATE TABLE " + TABLA_USUARIOS + " (" +  CAMPO_USUARIO + " TEXT NOT NULL PRIMARY KEY, " + CAMPO_CONTRASEÑA + " TEXT)";
    public static final String DROPEAR_TABLA_USUARIOS = "DROP TABLE IF EXISTS " + TABLA_USUARIOS;


    public boolean equals(Usuario u) {
        return this.usuario.equals(u.getUsuario()) && this.contraseña.equals(u.getContraseña());
    }



}
