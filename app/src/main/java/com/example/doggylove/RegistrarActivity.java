package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.BoringLayout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Usuario;
import com.example.doggylove.entidades.GestiónDeUsuarios;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class RegistrarActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText usuario,contraseña,repetircontraseña;
    private Button Registrarse;
    private TextView Alerta;
    private CheckBox validacion;
    NotificationCompat.Builder builder;
    Conexion conexion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        getSupportActionBar().hide();
        usuario = findViewById(R.id.editTextUsuario2);
        contraseña = findViewById(R.id.editTextContraseña2);
        repetircontraseña = findViewById(R.id.editTextContraseña3);
        Alerta = findViewById(R.id.textView7);
        validacion = findViewById(R.id.checkboxCondiciones);
        Registrarse = findViewById(R.id.buttonRegistrarse2);
        Registrarse.setOnClickListener(this);

        Intent i =  new Intent(this,IniciarSesionActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this,0,i,0);

        String ch_id = "0001";
        builder = new NotificationCompat.Builder(this,ch_id);
        builder.setContentTitle("Inicio de sesion");
        builder.setContentText("Se ha cerrado la sesión. Haga click aquí para iniciar nuevamente.");
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setContentIntent(pi);
        builder.setAutoCancel(true);

        conexion = new Conexion(this);

    }

    @Override
    public void onClick(View view) {

        String rUsuario = usuario.getText().toString();
        String rContraseña = contraseña.getText().toString();

        if (view.getId() == R.id.buttonRegistrarse2 && validacion.isChecked()==false) {
            Toast.makeText(this, "Debe aceptar las condiciones", Toast.LENGTH_LONG).show();
        }

        if (view.getId() == R.id.buttonRegistrarse2 && validacion.isChecked()==true) {

            if (rContraseña.equals("") || repetircontraseña.getText().toString().equals("") || rUsuario.equals("")) {
                Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
            }
            else if (!(rContraseña.equals(repetircontraseña.getText().toString()))) {
                Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
                contraseña.setText("");
                repetircontraseña.setText("");

            } else {

                if (validarUsuario(rUsuario)) {
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                    int id=1;
                    notificationManager.notify(id,builder.build());

                    Intent i = new Intent(this, RegistrarPerro.class);
                    Bundle extras = new Bundle();
                    extras.putSerializable("usuario",new Usuario(rUsuario,rContraseña));
                    i.putExtras(extras);
                    startActivity(i);
                } else {
                    Toast.makeText(this, "Nombre de usuario ya registrado. Intente con otro", Toast.LENGTH_LONG).show();
                    usuario.setText("");
                    contraseña.setText("");
                    repetircontraseña.setText("");
                }


                /*
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_USUARIO,rUsuario);
                values.put(Usuario.CAMPO_CONTRASEÑA,rContraseña);

                long insert = db.insert(Usuario.TABLA_USUARIOS,Usuario.CAMPO_USUARIO,values);

                if (insert>-1) {
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                    int id=1;
                    notificationManager.notify(id,builder.build());

                    Intent i = new Intent(this, RegistrarPerro.class);
                    Bundle extras = new Bundle();
                    extras.putSerializable("usuario",new Usuario(rUsuario,rContraseña));
                    i.putExtras(extras);
                    startActivity(i);
                } else {
                    Toast.makeText(this, "Nombre de usuario ya registrado. Intente con otro", Toast.LENGTH_LONG).show();
                    usuario.setText("");
                    contraseña.setText("");
                    repetircontraseña.setText("");
                }

                 */
            }
        }
    }

    private boolean validarUsuario(String usuario) {
        boolean a = true;
        List<String> listaNombres = new LinkedList<String>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_USUARIO,Usuario.CAMPO_CONTRASEÑA};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,null,null,null,null,Usuario.CAMPO_USUARIO);

        while (cursor.moveToNext()) {
            Usuario u = new Usuario(cursor.getString(0),cursor.getString(1));
            listaNombres.add(u.getUsuario());
        }

        for (int i=0;i<listaNombres.size();i++) {
            if (usuario.equals(listaNombres.get(i))) {
                a =false;
                break;
            }
        }
        return a;
    }

}