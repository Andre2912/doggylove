package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

public class ConfiguracionActivity extends AppCompatActivity implements View.OnClickListener {
    RadioButton cambiarContraseña,cerrarSesion,eliminarCuenta;
    Button cambiar,seguroEliminar,seguroCerrar;
    EditText nuevaContraseña;
    TextView seguro;
    Conexion conexion;
    private Usuario usuario;
    private Boolean recordarsesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        getSupportActionBar().hide();

        seguroCerrar = findViewById(R.id.buttonSeguroC);
        seguroEliminar = findViewById(R.id.buttonSeguroE);
        seguro = findViewById(R.id.textViewSeguro);
        cambiar = findViewById(R.id.buttonCambiar);
        nuevaContraseña = findViewById(R.id.editTextTextPersonName);
        cambiarContraseña = findViewById(R.id.radioButtonCambiarContraseña);
        cerrarSesion = findViewById(R.id.radioButtonCerrarSesion);
        eliminarCuenta = findViewById(R.id.radioButtonEliminarCuenta);

        seguroEliminar.setOnClickListener(this);
        seguroCerrar.setOnClickListener(this);
        cambiar.setOnClickListener(this);
        cambiarContraseña.setOnClickListener(this);
        cerrarSesion.setOnClickListener(this);
        eliminarCuenta.setOnClickListener(this);

        conexion = new Conexion(this);
    }

    @Override
    public void onClick(View view) {
        Perro perro1;
        SharedPreferences sharedPreferences = getSharedPreferences("doggylove",MODE_PRIVATE);
        recordarsesion = sharedPreferences.getBoolean("recordarsesion",false);

        if (recordarsesion) {
            usuario = new Usuario(sharedPreferences.getString("usuario_nombre",""),sharedPreferences.getString("usuario_contraseña",""));
        } else {
            Bundle extra = getIntent().getExtras();
            perro1 = (Perro) extra.getSerializable("perro");
            usuario = perro1.getUsuario();
        }

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Perro.CAMPO_ID, Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO};
        String[] valoresWhere1 = {usuario.getUsuario()};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_USUARIO+"=?",valoresWhere1,null,null,null);
        cursor.moveToFirst();
        Perro perro = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),usuario);


        SQLiteDatabase db1 = conexion.getWritableDatabase();
        String[] valoresWhere = {usuario.getUsuario()};
        String where = Usuario.CAMPO_USUARIO + "=?";
        String where1 = Perro.CAMPO_USUARIO + "=?";

        if (caso() == 1) {
            seguro.setVisibility(View.INVISIBLE);
            seguroCerrar.setVisibility(View.INVISIBLE);
            seguroEliminar.setVisibility(View.INVISIBLE);

            cambiar.setVisibility(View.VISIBLE);
            nuevaContraseña.setVisibility(View.VISIBLE);
        }

        if (caso() == 2) {
            cambiar.setVisibility(View.INVISIBLE);
            nuevaContraseña.setVisibility(View.INVISIBLE);
            seguroCerrar.setVisibility(View.INVISIBLE);

            seguro.setText("¿Está seguro que desea eliminar su cuenta?");
            seguro.setVisibility(View.VISIBLE);
            seguroEliminar.setVisibility(View.VISIBLE);
        }
        if (caso() == 3) {
            cambiar.setVisibility(View.INVISIBLE);
            nuevaContraseña.setVisibility(View.INVISIBLE);
            seguroEliminar.setVisibility(View.INVISIBLE);

            seguro.setText("¿Está seguro que desea cerrar sesión?");
            seguro.setVisibility(View.VISIBLE);
            seguroCerrar.setVisibility(View.VISIBLE);
        }

        if (view.getId() == R.id.buttonSeguroC) {
            recordarsesion = false;
            SharedPreferences.Editor editor = getSharedPreferences("doggylove",MODE_PRIVATE).edit();
            editor.putBoolean("recordarsesion",recordarsesion);
            editor.commit();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        if (view.getId() == R.id.buttonSeguroE) {
            recordarsesion = false;
            SharedPreferences.Editor editor = getSharedPreferences("doggylove",MODE_PRIVATE).edit();
            editor.putBoolean("recordarsesion",recordarsesion);
            editor.commit();
            int delete = db1.delete(Usuario.TABLA_USUARIOS, where, valoresWhere);
            int delete1 = db1.delete(Perro.TABLA_PERROS,where1,valoresWhere);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        if (view.getId() == R.id.buttonCambiar) {
            if (nuevaContraseña.getText().toString().equals("")) {
                Toast.makeText(this,"Debe colocar una nueva contraseña",Toast.LENGTH_LONG).show();
            } else if (usuario.getContraseña().equals(nuevaContraseña.getText().toString())) {
                Toast.makeText(this,"Su contraseña debe ser diferente a la actual",Toast.LENGTH_LONG).show();
            } else {
                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_CONTRASEÑA, nuevaContraseña.getText().toString());
                int update = db1.update(Usuario.TABLA_USUARIOS, values, where, valoresWhere);
                Toast.makeText(this,"Su contraseña ha sido cambiada con éxito",Toast.LENGTH_LONG).show();
            }

        }
    }

    private int caso() {
        int a=0;
        if (cambiarContraseña.isChecked()) {
            a=1;
        }
        if (eliminarCuenta.isChecked()) {
            a=2;
        }
        if (cerrarSesion.isChecked()) {
            a=3;
        }
        return a;
    }

}