package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RegistrarPerro extends AppCompatActivity implements View.OnClickListener , AdapterView.OnItemSelectedListener {
    TextView bienvenida;
    private Spinner sexo,tamaño;
    private List<String> sexos,tamaños;
    private EditText nombre,edad,raza;
    private Button Siguiente;
    private String rSexo = "";
    private String rTamaño = "";
    Conexion conexion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_perro);
        getSupportActionBar().hide();

        nombre = findViewById(R.id.editTextTextNombre);
        edad = findViewById(R.id.editTextTextEdad);
        raza = findViewById(R.id.editTextTextRaza);
        Siguiente = findViewById(R.id.buttonSiguiente);
        Siguiente.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        Usuario u = (Usuario) extras.getSerializable("usuario");

        bienvenida = findViewById(R.id.textView8);
        bienvenida.setText("Bienvenid@ " + u.getUsuario() + "!");

        sexo = findViewById(R.id.spinnerSexo);
        tamaño = findViewById(R.id.spinnerTamaño);

        sexos = new ArrayList<>();
        sexos.add("");
        sexos.add("Macho");
        sexos.add("Hembra");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,sexos);
        sexo.setAdapter(adapter1);

        tamaños = new ArrayList<>();
        tamaños.add("");
        tamaños.add("Pequeño");
        tamaños.add("Mediano");
        tamaños.add("Grande");
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,tamaños);
        tamaño.setAdapter(adapter2);

        sexo.setOnItemSelectedListener(this);
        tamaño.setOnItemSelectedListener(this);

        conexion = new Conexion(this);

    }

    @Override
    public void onClick(View view) {
        String rNombre = nombre.getText().toString();
        String rEdad = edad.getText().toString();
        String rRaza = raza.getText().toString();

        Bundle extras = getIntent().getExtras();
        Usuario u = (Usuario) extras.getSerializable("usuario");


        if (view.getId() == R.id.buttonSiguiente) {
            if (rNombre.equals("") || rEdad.equals("") || rRaza.equals("") || rSexo.equals("") || rTamaño.equals("")) {
                Toast.makeText(this,"Debe llenar todos los datos",Toast.LENGTH_LONG).show();
            } else {

                Intent intent = new Intent(this,SubirImagen.class);
                Bundle extra = new Bundle();
                extra.putSerializable("perro", new Perro(nuevaId(),rNombre,Integer.parseInt(rEdad),rRaza,rSexo,rTamaño,u));
                intent.putExtras(extra);
                startActivity(intent);

            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        String v = adapterView.getItemAtPosition(i).toString();

        if (adapterView.getId() == R.id.spinnerSexo) {
            rSexo = v;
        } else if (adapterView.getId() == R.id.spinnerTamaño) {
            rTamaño = v;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private int consultarId(Usuario usuario) {
        int a;
        String b;
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] valoresWhere = {usuario.getUsuario()};
        String[] valores = {Perro.CAMPO_ID};

        Cursor cursor = db.query(Perro.TABLA_PERROS,valores,Perro.CAMPO_USUARIO+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        b = cursor.getString(0);
        a = Integer.parseInt(b);
        cursor.close();
        return a;
    }

    List<Integer> listaPerros;
    private int nueva() {

        listaPerros = new LinkedList<Integer>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Perro.CAMPO_ID,Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO,Perro.CAMPO_USUARIO};

        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,null,null,null,null,Perro.CAMPO_ID);

        while (cursor.moveToNext()) {
            int a = cursor.getInt(0);
            listaPerros.add(a);
        }
        return listaPerros.size()+1;
    }

    private int nuevaId() {
        int a;
        List<String> listaNombres = new LinkedList<String>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_USUARIO,Usuario.CAMPO_CONTRASEÑA};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,null,null,null,null,Usuario.CAMPO_USUARIO);

        while (cursor.moveToNext()) {
            Usuario u = new Usuario(cursor.getString(0),cursor.getString(1));
            listaNombres.add(u.getUsuario());
        }

        a = listaNombres.size()+1;
        return a;
    }


}