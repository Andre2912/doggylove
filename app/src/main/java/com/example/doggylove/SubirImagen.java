package com.example.doggylove;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class SubirImagen extends AppCompatActivity implements View.OnClickListener {
    private TextView bienvenida;
    ImageView imagen;
    Bitmap foto = null;
    Conexion conexion;
    Button registrarse;
    TextView click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir_imagen);
        getSupportActionBar().hide();

        Bundle extra = getIntent().getExtras();
        Perro perro = (Perro)extra.getSerializable("perro");

        bienvenida = findViewById(R.id.textView15);
        bienvenida.setText("Suba una foto de " + perro.getNombre());

        click = findViewById(R.id.textView16);

        imagen = findViewById(R.id.imageView4);
        imagen.setOnClickListener(this);

        registrarse = findViewById(R.id.buttonFoto);
        registrarse.setOnClickListener(this);

        conexion = new Conexion(this);

    }

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;

    @Override
    public void onClick(View view) {
        Bundle extra = getIntent().getExtras();
        Perro perro = (Perro)extra.getSerializable("perro");
        boolean recordarsesion;

        if (view.getId() == imagen.getId()) {

            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(SubirImagen.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_STORAGE_PERMISSION);

            } else {

                seleccionarImagen();
            }
            click.setVisibility(View.INVISIBLE);
        }

        if (view.getId() == registrarse.getId() && foto!=null) {

            SQLiteDatabase db = conexion.getWritableDatabase();
            ContentValues values1 = new ContentValues();

            values1.put(Usuario.CAMPO_USUARIO,perro.getUsuario().getUsuario());
            values1.put(Usuario.CAMPO_CONTRASEÑA,perro.getUsuario().getContraseña());

            long insert1 = db.insert(Usuario.TABLA_USUARIOS,Usuario.CAMPO_USUARIO,values1);

            ContentValues values2 = new ContentValues();

            values2.put(Perro.CAMPO_ID,perro.getId());
            values2.put(Perro.CAMPO_NOMBRE,perro.getNombre() );
            values2.put(Perro.CAMPO_EDAD, perro.getEdad());
            values2.put(Perro.CAMPO_RAZA, perro.getRaza());
            values2.put(Perro.CAMPO_SEXO, perro.getSexo());
            values2.put(Perro.CAMPO_TAMAÑO, perro.getTamaño());

            if (foto!=null) {
                byte[] bytes;
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                this.foto.compress(Bitmap.CompressFormat.JPEG,100,out);
                bytes = out.toByteArray();
                values2.put(Perro.CAMPO_FOTO, bytes);
            }

            values2.put(Perro.CAMPO_USUARIO, perro.getUsuario().getUsuario());

            long insert2 = db.insert(Perro.TABLA_PERROS,Perro.CAMPO_ID,values2);

            if (insert2 > -1 ) {

                Intent i = new Intent(this,Home.class);
                Bundle extras = new Bundle();
                extras.putSerializable("perro",perro);
                i.putExtras(extras);
                startActivity(i);
            }

        } else if (view.getId() == registrarse.getId() && foto==null){
            Toast.makeText(this, "Debe subir una foto de su perro", Toast.LENGTH_SHORT).show();
        }

    }

    private void seleccionarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent,REQUEST_CODE_SELECT_IMAGE);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION && grantResults.length>0) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarImagen();
            }
            else {
                Toast.makeText(this,"Permiso denegado",Toast.LENGTH_SHORT).show();
            }

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {

            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectedImageUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectedImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    imagen.setImageBitmap(bitmap);
                    this.foto = bitmap;

                } catch (FileNotFoundException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

        }
    }

}