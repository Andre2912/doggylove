package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Boolean recordarsesion;

    Button buttonIniciarSesion,buttonRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonIniciarSesion = findViewById(R.id.buttonIniciarSesion);
        buttonRegistrarse = findViewById(R.id.buttonRegistrarse);
        buttonIniciarSesion.setOnClickListener(this);
        buttonRegistrarse.setOnClickListener(this);
        getSupportActionBar().hide();

        SharedPreferences sharedPreferences = getSharedPreferences("doggylove",MODE_PRIVATE);
        recordarsesion = sharedPreferences.getBoolean("recordarsesion",false);

        if (recordarsesion) {
            Intent intent = new Intent(this,Home.class);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonRegistrarse) {
            Intent i = new Intent(this,RegistrarActivity.class);
            startActivity(i);
        }

        if (view.getId() == R.id.buttonIniciarSesion) {
            Intent i = new Intent(this,IniciarSesionActivity.class);
            startActivity(i);
        }
    }


}