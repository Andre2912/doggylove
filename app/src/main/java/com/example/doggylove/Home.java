package com.example.doggylove;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.doggylove.BaseDatos.Conexion;
import com.example.doggylove.BaseDatos.Perro;
import com.example.doggylove.BaseDatos.Usuario;

import org.imaginativeworld.whynotimagecarousel.model.CarouselItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Home extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private TextView Bienvenida;
    private ImageView Configuracion;
    private Conexion conexion;
    private Usuario usuario;
    private  Boolean recordarsesion;
    private Perro perro;
    ListView lista;
    ArrayAdapter<String> adaptador;
    byte[] blob;
    private ImageView perfil;
    private TextView nombre, edad, raza, sexo, tamaño;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        Bienvenida = findViewById(R.id.textViewBienvenida);
        Configuracion = findViewById(R.id.imageViewConfiguracion);
        Configuracion.setOnClickListener(this);
        perfil = findViewById(R.id.imageView7);

        nombre = findViewById(R.id.textView20);
        edad = findViewById(R.id.textView19);
        raza = findViewById(R.id.textView21);
        sexo = findViewById(R.id.textView18);
        tamaño = findViewById(R.id.textView22);

        SharedPreferences sharedPreferences = getSharedPreferences("doggylove",MODE_PRIVATE);
        recordarsesion = sharedPreferences.getBoolean("recordarsesion",false);

        conexion = new Conexion(this);

        if (recordarsesion) {
            usuario = new Usuario(sharedPreferences.getString("usuario_nombre",""),sharedPreferences.getString("usuario_contraseña",""));

            SQLiteDatabase db = conexion.getReadableDatabase();
            String[] campos = {Perro.CAMPO_ID, Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO};
            String[] valoresWhere = {usuario.getUsuario()};
            Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_USUARIO+"=?",valoresWhere,null,null,null);
            cursor.moveToFirst();
            perro = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),usuario);

        } else {
            Bundle extras = getIntent().getExtras();
            perro = (Perro)extras.getSerializable("perro");
            usuario = perro.getUsuario();
        }

        Bienvenida.setText("Bienvenid@ " + usuario.getUsuario() + "!");

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Perro.CAMPO_FOTO};
        String[] valoresWhere = {perro.getId()+""};
        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_ID+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        if (cursor.getBlob(0) !=null) {
            this.blob = cursor.getBlob(0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blob,0, blob.length);
            perfil.setImageBitmap(bitmap);
        }

        nombre.setText("Nombre: " + perro.getNombre());
        edad.setText("Edad: " + perro.getEdad() + " año(s)");
        raza.setText("Raza: " + perro.getRaza());
        sexo.setText("Sexo: " + perro.getSexo());
        tamaño.setText("Tamaño: " + perro.getTamaño());


        lista = (ListView)findViewById(R.id.listViewPerros);

        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,getLista(perro.getSexo()));
        lista.setAdapter(adaptador);
        lista.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Perro perro;

        SharedPreferences sharedPreferences = getSharedPreferences("doggylove",MODE_PRIVATE);
        recordarsesion = sharedPreferences.getBoolean("recordarsesion",false);
        if (recordarsesion) {
            usuario = new Usuario(sharedPreferences.getString("usuario_nombre",""),sharedPreferences.getString("usuario_contraseña",""));
            SQLiteDatabase db = conexion.getReadableDatabase();
            String[] campos = {Perro.CAMPO_ID, Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO};
            String[] valoresWhere = {usuario.getUsuario()};
            Cursor cursor = db.query(Perro.TABLA_PERROS,campos,Perro.CAMPO_USUARIO+"=?",valoresWhere,null,null,null);
            cursor.moveToFirst();
            perro = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),usuario);
        } else {
            Bundle extras = getIntent().getExtras();
            perro = (Perro)extras.getSerializable("perro");
            usuario = new Usuario(perro.getUsuario().getUsuario(),perro.getUsuario().getContraseña());
        }

        if (view.getId() == R.id.imageViewConfiguracion) {
            Intent intent = new Intent(this,ConfiguracionActivity.class);
            Bundle extra = new Bundle();
            extra.putSerializable("perro",perro);

            intent.putExtras(extra);
            startActivity(intent);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int usuarioId = listaPerros.get(i).getId();

        Intent intent = new Intent(this,ContactarPerro.class);
        Bundle extras = new Bundle();
        extras.putInt("id",usuarioId);
        intent.putExtras(extras);
        startActivity(intent);
    }

    List<Perro> listaPerros;

    private List<String> getLista(String sexo) {

        listaPerros = new LinkedList<Perro>();

        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Perro.CAMPO_ID,Perro.CAMPO_NOMBRE,Perro.CAMPO_EDAD,Perro.CAMPO_RAZA,Perro.CAMPO_SEXO,Perro.CAMPO_TAMAÑO,Perro.CAMPO_FOTO,Perro.CAMPO_USUARIO};

        Cursor cursor = db.query(Perro.TABLA_PERROS,campos,null,null,null,null,Perro.CAMPO_ID);

        while (cursor.moveToNext()) {

            Perro u = new Perro(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),null);
            if (u.getSexo().equals(sexo)) {

            } else {
                listaPerros.add(u);
                listaNombres.add(u.getNombre());
            }

        }
        return listaNombres;
    }
}